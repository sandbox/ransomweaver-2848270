<?php

namespace Drupal\commerce_stock_backorderable;

use Drupal\commerce_stock\StockAvailabilityChecker;
use Drupal\commerce\AvailabilityCheckerInterface;
use Drupal\commerce\AvailabilityResponse;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce\Context;

/**
 * The entry point for availability checking through Commerce Stock.
 *
 * Proxies requests to stock services configured for each entity.
 *
 * @package Drupal\commerce_stock
 */
class BackorderableStockAvailabilityChecker extends StockAvailabilityChecker implements AvailabilityCheckerInterface {

  /**
   * {@inheritdoc}
   */
  public function check(PurchasableEntityInterface $entity, $quantity, Context $context) {
    $stock_service = $this->stockServiceManager->getService($entity);
    $stock_checker = $stock_service->getStockChecker();
    $stock_config = $stock_service->getConfiguration();
    $locations = $stock_config->getAvailabilityLocations($this->stockServiceManager->getContext($entity), $entity);

    if ($stock_checker->getIsAlwaysInStock($entity)) {
      return AvailabilityResponse::available(0, $quantity);
    }
    $no_stock_sale_field = $entity->get('allow_no_stock_sale');
    if ($no_stock_sale_field) {
      $allow_no_stock = $no_stock_sale_field->value;
      if (boolval($allow_no_stock)) {
        return AvailabilityResponse::available(0, $quantity);
      }
    }

    $stock_level = $stock_checker->getTotalStockLevel($entity, $locations);
    // @todo Minimum qty instead of 0?

    if ($stock_level >= $quantity) {
      return AvailabilityResponse::available(0, $stock_level);
    }
    else {
      return AvailabilityResponse::unavailable(0, $stock_level, 'maximum exceeded');
    }
  }

}
