<?php

namespace Drupal\commerce_stock_backorderable\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\LocalStockChecker;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Performs stock transactions on order and order item events.
 */
class BackorderableOrderEventSubscriber extends OrderEventSubscriber implements EventSubscriberInterface {


  /**
   * Creates a stock transaction when an order is placed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The order workflow event.
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    foreach ($order->getItems() as $item) {
      $entity = $item->getPurchasedEntity();
      $service = $this->stockServiceManager->getService($entity);
      $checker = $service->getStockChecker();
      if ($checker->getIsStockManaged($entity)) {
        // transact a reduction of actual stock available, not what is in the possibly partially backorderable item quantity
        $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
        $quantity = -1 * min($original_stock_available, $item->getQuantity());
        $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $quantity);
        $metadata = [
          'related_oid' => $order->id(),
          'related_uid' => $order->getCustomerId(),
          'data' => ['message' => 'order placed'],
        ];
        $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, StockTransactionsInterface::STOCK_SALE, $metadata);
        $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
        \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
      }
    }
  }

  /**
   * Acts on the order update event to create transactions for new items.
   *
   * The reason this isn't handled by OrderEvents::ORDER_ITEM_INSERT is because
   * that event never has the correct values.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order event.
   */
  public function onOrderUpdate(OrderEvent $event) {
    $order = $event->getOrder();
    $original_order = $order->original;
    foreach ($order->getItems() as $item) {
      if (!$original_order->hasItem($item)) {
        if ($order && !in_array($order->getState()->value, ['draft', 'canceled'])) {
          $entity = $item->getPurchasedEntity();
          $service = $this->stockServiceManager->getService($entity);
          $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $item->getQuantity());
          // transact a reduction of actual stock available, not what is in the possibly partially backorderable item quantity
          $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
          $quantity = -1 * min($original_stock_available, $item->getQuantity());
          $metadata = [
            'related_oid' => $order->id(),
            'related_uid' => $order->getCustomerId(),
            'data' => ['message' => 'order item added'],
          ];
          $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, StockTransactionsInterface::STOCK_SALE, $metadata);
          $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
          \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
        }
      }
    }
  }

  /**
   * Performs a stock transaction for an order Cancel event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The order workflow event.
   */
  public function onOrderCancel(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    foreach ($order->getItems() as $item) {
      $entity = $item->getPurchasedEntity();
      $service = $this->stockServiceManager->getService($entity);
      $checker = $service->getStockChecker();
      if ($checker->getIsStockManaged($entity)) {
        // we need to find out how much stock was withdrawn for this order, and return THAT amount, not the quantity of the item in the order
        $quantity = 0;
        if ($checker instanceof LocalStockChecker) {
          $quantity = -1 * $checker->getOrderStockTransactionSum($order->id(), $entity->id());
        }
        $quantity = max($quantity, 0); // sanity check; a cancellation can't withdraw stock, so the larger of quantity or zero.
        $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $quantity);
        $metadata = [
          'related_oid' => $order->id(),
          'related_uid' => $order->getCustomerId(),
          'data' => ['message' => 'order canceled'],
        ];
        $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
        $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, StockTransactionsInterface::STOCK_RETURN, $metadata);
        $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
        \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
      }
    }
  }

  /**
   * Performs a stock transaction on an order delete event.
   *
   * This happens on PREDELETE since the items are not available after DELETE.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order event.
   */
  public function onOrderDelete(OrderEvent $event) {
    $order = $event->getOrder();
    if ($order->getState()->value == 'canceled') {
      return;
    }
    $items = $order->getItems();
    foreach ($items as $item) {
      $entity = $item->getPurchasedEntity();
      $service = $this->stockServiceManager->getService($entity);
      $checker = $service->getStockChecker();
      if ($checker->getIsStockManaged($entity)) {
        // we need to find out how much stock was withdrawn for this order, and return THAT amount, not the quantity of the item in the order
        $quantity = 0;
        if ($checker instanceof LocalStockChecker) {
          $quantity = -1 * $checker->getOrderStockTransactionSum($order->id(), $entity->id());
        }
        $quantity = max($quantity, 0); // sanity check; a cancellation can't withdraw stock, so the larger of quantity or zero.
        $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $quantity);
        $metadata = [
          'related_oid' => $order->id(),
          'related_uid' => $order->getCustomerId(),
          'data' => ['message' => 'order deleted'],
        ];
        $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
        $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, StockTransactionsInterface::STOCK_RETURN, $metadata);
        $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
        \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
      }
    }
  }

  /**
   * Performs a stock transaction on an order item update.
   *
   * @param \Drupal\commerce_order\Event\OrderItemEvent $event
   *   The order item event.
   */
  public function onOrderItemUpdate(OrderItemEvent $event) {
    $item = $event->getOrderItem();
    $order = $item->getOrder();
    if ($order && !in_array($order->getState()->value, ['draft', 'canceled'])) {
      $diff = $item->original->getQuantity() - $item->getQuantity();
      if ($diff) {
        $entity = $item->getPurchasedEntity();
        $service = $this->stockServiceManager->getService($entity);
        $quantity = $diff;
        if  ($quantity < 0) {
          $checker = $service->getStockChecker();
          $stock_actually_withdrawn = 0;
          if ($checker instanceof LocalStockChecker) {
            $stock_actually_withdrawn = $checker->getOrderStockTransactionSum($order->id(), $entity->id());
          }
          if (-1 * $quantity > $stock_actually_withdrawn) {
            $quantity = -1 * $stock_actually_withdrawn;
          }
          $quantity = max($quantity, 0); // sanity check; a return can't withdraw stock
          $transaction_type = StockTransactionsInterface::STOCK_RETURN;
        } else {
          $actual_stock_available = $this->stockServiceManager->getStockLevel($entity);
          $quantity = -1 * min($actual_stock_available, $quantity);
          $transaction_type = StockTransactionsInterface::STOCK_SALE;
        }
        $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $quantity);
        $metadata = [
          'related_oid' => $order->id(),
          'related_uid' => $order->getCustomerId(),
          'data' => ['message' => 'order item quantity updated'],
        ];
        $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
        $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, $transaction_type, $metadata);
        $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
        \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
      }
    }
  }

  /**
   * Performs a stock transaction when an order item is deleted.
   *
   * @param \Drupal\commerce_order\Event\OrderItemEvent $event
   *   The order item event.
   */
  public function onOrderItemDelete(OrderItemEvent $event) {
    $item = $event->getOrderItem();
    $order = $item->getOrder();
    if ($order && !in_array($order->getState()->value, ['draft', 'canceled'])) {
      $entity = $item->getPurchasedEntity();
      $service = $this->stockServiceManager->getService($entity);
      $checker = $service->getStockChecker();
      // we need to find out how much stock was withdrawn for this order, and return THAT amount, not the quantity of the item in the order
      $quantity = 0;
      if ($checker instanceof LocalStockChecker) {
        $quantity = -1 * $checker->getOrderStockTransactionSum($order->id(), $entity->id());
      }
      $quantity = min($quantity, 0); // sanity check; a cancellation can't withdraw stock, so the smaller of quantity or zero.
      $location = $this->stockServiceManager->getTransactionLocation($this->stockServiceManager->getContext($entity), $entity, $quantity);
      $metadata = [
        'related_oid' => $order->id(),
        'related_uid' => $order->getCustomerId(),
        'data' => ['message' => 'order item deleted'],
      ];
      $original_stock_available = $this->stockServiceManager->getStockLevel($entity);
      $service->getStockUpdater()->createTransaction($entity, $location->id(), '', $quantity, NULL, StockTransactionsInterface::STOCK_RETURN, $metadata);
      $new_stock_available = $this->stockServiceManager->getStockLevel($entity);
      \Drupal::moduleHandler()->invokeAll('stock_level_changed', [$new_stock_available, $original_stock_available, $entity]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // State change events fired on workflow transitions from state_machine.
      'commerce_order.place.post_transition' => ['onOrderPlace', -100],
      'commerce_order.cancel.post_transition' => ['onOrderCancel', -100],
      // Order storage events dispatched during entity operations in CommerceContentEntityStorage.
      // ORDER_UPDATE handles new order items since ORDER_ITEM_INSERT doesn't.
      OrderEvents::ORDER_UPDATE => ['onOrderUpdate', -100],
      OrderEvents::ORDER_PREDELETE => ['onOrderDelete', -100],
      OrderEvents::ORDER_ITEM_UPDATE => ['onOrderItemUpdate', -100],
      OrderEvents::ORDER_ITEM_DELETE => ['onOrderItemDelete', -100],
    ];
    return $events;
  }

}
