<?php

namespace Drupal\commerce_stock_backorderable\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\PurchasableEntityInterface;

/**
 * Plugin implementation of the 'stock_field_formatter_backorderable' formatter.
 *
 * @FieldFormatter(
 *   id = "stock_field_formatter_backorderable",
 *   module = "commerce_stock_field",
 *   label = @Translation("Backorderable stock formatter"),
 *   field_types = {
 *     "commerce_stock_level"
 *   }
 * )
 */
class BackorderableStockFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
  return [
    'shopinde_stockfield_title' => '',
    ] + parent::defaultSettings();
  }

  /**
 * Returns a form to configure settings for the formatter.
 *
 * Invoked from \Drupal\field_ui\Form\EntityDisplayFormBase to allow
 * administrators to configure the formatter. The field_ui module takes care
 * of handling submitted form values.
 *
 * @param array $form
 * The form where the settings form is being included in.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * The current state of the form.
 *
 * @return array
 * The form elements for the formatter settings.
 */
public function settingsForm(array $form, FormStateInterface $form_state) {
  $form = parent::settingsForm($form, $form_state);

  $form['backorderable_stockfield_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Stock field title'),
    '#description' => t('Enter a custom title'),
    '#default_value' => $this->getSetting('backorderable_stockfield_title'),
  );

  return $form;
}

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Customize the field label.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $entity = $items->getEntity();
    if ($entity instanceof PurchasableEntityInterface) {
      $stockServiceManager = \Drupal::service('commerce_stock.service_manager');
      $level = $stockServiceManager->getStockLevel($entity);

      if ($level == 0) {
        $allow_no_stock = $entity->get('allow_no_stock_sale')->value;
        $message = $entity->get('no_stock_message')->value;
        if (boolval($allow_no_stock)) {
          if (empty($message)) {
            $level = t('Custom order - delivery time unknown');
          } else {
            $level = t($message);
          }
        } else {
          $level = t("Out of stock");
        }
      } else if ($level > 10) {
        $level = t('In stock');
      } else {
        $level = t('%ct remaining', ['%ct' => $level]);
      }

      if ($this->getSetting('backorderable_stockfield_title') == NULL) {
        $label = t("Availability");
      } else {
        $label =  $this->getSetting('backorderable_stockfield_title');
      }

    } else {
      // No stock if this is not purchaseble
      $level = 0;
      $label = '';
    }


    $elements[0 ] = ['#markup' => '<strong>' . $label . ':</strong> ' . $level];

    return $elements;
  }

}
